import sqlite3
from flask import Flask, jsonify, abort, request

app = Flask(__name__)

conn = sqlite3.connect('temp.db')
c = conn.cursor()

def create_table():
    c.execute("CREATE TABLE IF NOT EXISTS stuffToSave(json_data TEXT, timestamp)")

def data_entry(data):
    c.execute("INSERT INTO stuffToSave (json_data) VALUES (?)", (data))
    conn.commit()
    c.close()
    conn.close()

@app.route('/api/v1', methods=['POST'])
def save_json():
    if not request.json:
        abort(400)

    try:
        data_entry(str(request.json))
    except:
        abort(400)

    return jsonify(request.json)

if __name__ == '__main__':

    create_table()
    app.run()

# docker build -t backend-api .
# docker run -e
# curl -i -H "Content-Type: application/json" -X POST -d '{"json":"whatever json this is"}' http://127.0.0.1:5000/api/v1
